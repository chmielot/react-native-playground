import AppNavigator from '../Navigation/AppNavigator'

export const reducer = (state, action) => {
  const newState = AppNavigator.router.getStateForAction(action, state)
  return newState || state
}
