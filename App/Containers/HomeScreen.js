import React from 'react'
import { ScrollView, Text, Button } from 'react-native'
import { connect } from 'react-redux'
// import { NavigationActions } from 'react-navigation'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/HomeScreenStyle'

class HomeScreen extends React.Component {

  // constructor (props) {
  //   super(props)
  //   this.state = {}
  // }
  static navigationOptions = {
    title: 'Welcome'
  };

  render () {
    const { navigate } = this.props.navigation
    return (
      <ScrollView style={styles.container}>
        <Text>HomeScreen Container</Text>
        <Button
          onPress={() => navigate('Test')}
          // onPress={() => dispatch(NavigationActions.navigate({ routeName: 'Test' })) }
          // onPress={() => NavigationActions.navigate({ routeName: 'Test' }) }
          title='Go to Testscreen'
        />
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
