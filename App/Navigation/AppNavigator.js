import { StackNavigator } from 'react-navigation'
import HomeScreen from '../Containers/HomeScreen'
import TestScreen from '../Containers/TestScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  Home: { screen: HomeScreen },
  Test: {
    screen: TestScreen,
    navigationOptions: { title: 'Test' }
  }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'Home',
  navigationOptions: {
    header: {
      style: styles.header
    }
  }
})

export default PrimaryNav
